<?php
require_once __DIR__ . '/../config.php';

require_once __DIR__.'/../vendor/autoload.php';

//подключаем  Silex и передаем в него какие-то конфиги
$app = new Silex\Application();

//подключаем графический интерпритатор

$app->register(new Silex\Provider\TwigServiceProvider(), array(
	'twig.path' => DIR_CATALOG.'view/',
));

//подключаем переводчик
$app->register(new Silex\Provider\LocaleServiceProvider());
$app->register(new Silex\Provider\TranslationServiceProvider(), array(
	'locale_fallbacks' => array('ru'),
));

use Symfony\Component\Translation\Loader\YamlFileLoader;

$app->extend('translator', function($translator, $app) {
	$translator->addLoader('yaml', new YamlFileLoader());

	$translator->addResource('yaml', DIR_CATALOG.'locales/ru.yml', 'ru');
	$translator->addResource('yaml', DIR_CATALOG.'locales/en.yml', 'en');


	return $translator;
});

//инициируем поддержку сесий

$app->register(new Silex\Provider\SessionServiceProvider());

//Инициализируем поддержку отправки писем

$app->register(new Silex\Provider\SwiftmailerServiceProvider());

$app['swiftmailer.options'] = array(
	'host' => MAIL_HOST,
	'port' => MAIL_PORT,
	'username' => MAIL_USER,
	'password' => MAIL_PASS,
	'encryption' => null,
	'auth_mode' => null
);


//подключаем и настраиваем базу данных
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
	'db.options' => array (
			'driver'    => 'pdo_mysql',
			'host'      => DB_HOST,
			'dbname'    => DB_NAME,
			'user'      => DB_USER,
			'password'  => DB_PASS,
			'charset'   => 'utf8mb4',
	),
));

//Подключаем логирование
$app->register(new Silex\Provider\MonologServiceProvider(), array(
	'monolog.logfile' => DIR_ROOT.'/log.log',
));

//Пишем роуты и присваиваем им имена.
$app->get('/', 'controller\index::home')->bind('homepage');

$app->get('/video.html', 'controller\video::index')->bind('video');

$app->get('/photo.html', 'controller\photo::index')->bind('photo');

$app->get('/fan.html', 'controller\fan::index')->bind('fan');

$app->get('/about.html', 'controller\about::index')->bind('about');

$app->get('/reg.html', 'controller\reg::index')->bind('reg');

$app->post('/reg/add.html', 'controller\reg::UserAdd')->bind('add');
$app->get('/reg/suc.html', 'controller\reg::Suc')->bind('suc');
$app->post('/reg/find.html', 'controller\reg::FindUser')->bind('find');
$app->post('/reg/edit.html', 'controller\reg::editUser')->bind('edit');
$app->post('/reg/upl.html', 'controller\reg::fotoUpload')->bind('upl');

$app->get('/pay.html', 'controller\pay::payStatus')->bind('pay');
$app->post('/pay-add.html', 'controller\pay::payAdd')->bind('pay-add');

$app['debug'] = true;

$app->run();