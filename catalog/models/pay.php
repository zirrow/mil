<?php
namespace model;

class pay {

	public function GetPayId($app) {
		$sql = "SELECT `value` FROM `settings` WHERE `setting_name`= 'pay_id'";

		$result = $app['db']->fetchArray($sql);

		return $result[0];
	}

	public function addPay($val, $app) {

		$app['db']->insert("tickets", $val);

		$sql = "UPDATE `users` SET `payStatus` = ? WHERE `id` = ?";

		$app['db']->executeUpdate($sql, array('1',$val['user_id']));

		return true;
	}

}