<?php
namespace model;

use Silex\Application;

class cron
{
	/**
	 * @var Application
	 */
	private $app;

	/**
	 * cron constructor.
	 * @param Application $app
	 */
	public function __construct(Application $app)
	{
		$this->app = $app;
	}

	/**
	 * @param int $seconds
	 * @return array
	 */
	public function get_settings($field)
	{
		$sql = 'SELECT `value`,`serialize`'
			. ' FROM `settings`'
			. ' WHERE `name` = ?'
			. ' LIMIT 1';

		$result = $this->app['db']->fetchAssoc($sql, array((string)$field));

		if($result['serialize']){
			$value = json_decode($result['value'], true);
		} else {
			$value = $result['value'];
		}

		return $value;
	}

	/**
	 * @param $field
	 * @param $value
	 * @return mixed
	 */
	public function set_settings($field, $value)
	{
		$sql = 'UPDATE `settings`'
			. ' SET `value` = ?,'
			. ' `serialize` = ?'
			. ' WHERE `name` = ?';

		$serialize = 0;

		if(is_array($value) || is_object($value)){

			$value = json_encode($value);

			$serialize = 1;
		}

		return $this->app['db']->executeUpdate($sql, array((string)$value, (int)$serialize, (string)$field));
	}

	/**
	 * @param int $count
	 * @return array
	 */
	public function lottery_vips($count)
	{
		$result = array();
		$user_ids = array();

		$sql = 'SELECT `id`'
			. ' FROM `users`'
			. ' WHERE `vip` != 0';

		$data = $this->app['db']->fetchAll($sql);

		$data_count = count($data);

		if(0 !== $data_count){

			if($count > $data_count){
				$count = $data_count;
			}

			foreach ($data as $item) {
				$user_ids[] = $item['id'];
			}

			$rand = array_rand($user_ids, (int)$count);

			foreach ((array)$rand as $key) {
				$result[] = (int)$user_ids[$key];
			}
		}

		return $result;
	}

	/**
	 * @param int $count
	 * @return array
	 */
	public function lottery($count, $prev_date, $curr_date)
	{
		$result = array();
		$user_ids = array();

		//need next day after previous lottery
		$prev = new \DateTime($prev_date);

		$prev->modify('+ 1 day');

		$prev_date = $prev->format('Y-m-d');

		$sql = 'SELECT `t`.`user_id`'
			. ' FROM `tickets` t'
			. ' LEFT JOIN `users` u ON `t`.`user_id`=`u`.`id`'
			. ' WHERE `t`.`date` BETWEEN ? AND ?'
			. ' AND `u`.`vip` = 0';

		$data = $this->app['db']->fetchAll($sql, array((string)$prev_date, (string)$curr_date));

		$data_count = count($data);

		if(0 !== $data_count){

			if($count > $data_count){
				$count = $data_count;
			}

			foreach ($data as $item) {
				$user_ids[] = $item['user_id'];
			}

			for ($i = 0; $i < $count; $i++) {

				if(0 === count($user_ids)){
					break;
				}

				$rand = array_rand($user_ids, 1);

				$res = (int)$user_ids[(int)$rand];

				$result[] = $res;

				//delete duplicates
				$duplicates = array_keys($user_ids, $res, false);

				if($duplicates){

					foreach ($duplicates as $duplicate) {

						unset($user_ids[$duplicate]);
					}
				}
			}
		}

		return $result;
	}

	/**
	 * @param string $lottery_date
	 * @return mixed
	 */
	public function add_new_lottery($lottery_date)
	{
		$this->app['db']->insert('lottery', array('date' => $lottery_date));

		return $this->app['db']->lastInsertId();
	}

	/**
	 * @param int $lottery_id
	 * @param array $winners
	 */
	public function set_lottery_winnes($lottery_id, array $winners)
	{
		//TODO - sql in cicle - might be optimized ??
		foreach ($winners as $winner) {

			$this->app['db']->insert('lottery_winners', array('lottery_id' => $lottery_id, 'user_id' => $winner));
		}

	}

	/**
	 * @param $lottery_id
	 * @param $curator_id
	 */
	public function set_lottery_curator($lottery_id, $curator_id)
	{
		$this->app['db']->update('lottery', array('curator_id' => (int)$curator_id), array('lottery_id' => (int)$lottery_id));
	}

	/**
	 * @return array
	 */
	public function get_free_curator()
	{
		$sql = 'SELECT `curator_id`,`firstName`,`lastName`,`email`'
			. ' FROM `lottery_curator`'
			. ' WHERE `free` = 1'
			. ' ORDER BY `free`'
			. ' LIMIT 1';

		return $this->app['db']->fetchAssoc($sql);
	}

	/**
	 * @param int $user_id
	 * @return array
	 */
	public function get_user_info($user_id)
	{
		$sql = 'SELECT *'
			. ' FROM `users`'
			. ' WHERE id = ?'
			. ' LIMIT 1';

		return $this->app['db']->fetchAssoc($sql, array((int)$user_id));
	}

	/**
	 * @param $warning_time
	 * @return array
	 */
	public function get_warning_users($warning_time)
	{
		$sql = 'SELECT `id`,`firstName`,`email`'
			. ' FROM `users`'
			. ' WHERE `reg_date` <= DATE_SUB(NOW(), INTERVAL ' . (int) $warning_time . ' SECOND) '
			. ' AND `warning` = 0'
			. ' AND `payStatus` = 0'
			. ' AND `vip` = 0';

		return $this->app['db']->fetchAll($sql);
	}

	/**
	 * @param array $user_list
	 */
	public function set_warning_label(array $warning_list)
	{
		$sql = 'UPDATE `users`'
			. ' SET `warning` = 1'
			. ' WHERE `id` IN ('. implode(',', $warning_list) .')';

		$this->app['db']->executeUpdate($sql);
	}

	/**
	 * @param $remove_time
	 * @return array
	 */
	public function get_remove_users($remove_time)
	{
		$sql = 'SELECT `id`,`firstName`,`email`,`photo`'
			. ' FROM `users`'
			. ' WHERE `reg_date` <= DATE_SUB(NOW(), INTERVAL ' . (int) $remove_time . ' SECOND) '
			. ' AND `payStatus` = 0'
			. ' AND `vip` = 0';

		return $this->app['db']->fetchAll($sql);
	}

	/**
	 * @param array $remove_photos
	 */
	public function remove_user_photo(array $remove_photos)
	{
		foreach ($remove_photos as $photo) {

			$filename = DIR_ROOT .'/web'. $photo;
			$thumnail = str_replace('/image/photos/', '/image/photos/thumbnail/', $filename);

			if(true === is_file($filename)){
				@unlink($filename);
			}

			if(true === is_file($thumnail)){
				@unlink($thumnail);
			}
		}
	}

	/**
	 * @param array $remove_list
	 */
	public function remove_user_account(array $remove_list)
	{
		$sql = 'DELETE'
			. ' FROM `users`'
			. ' WHERE `id` IN ('. implode(',', $remove_list) .')';

		$this->app['db']->executeUpdate($sql);
	}
}
