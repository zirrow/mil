<?php
namespace controller;

use Silex\Application;
use Silex\Route;

class cron
{
	/**
	 * @var Application
	 */
	private $app;

	/**
	 * @var \model\cron
	 */
	private $model;

	/**
	 * @var string
	 */
	private $curr_date;

	/**
	 * @var string
	 */
	private $prev_date;

	/**
	 * cron constructor.
	 * @param Application $app
	 */
	public function __construct(Application $app)
	{
		$this->app = $app;

		$this->model = new \model\cron($this->app);
	}

	/**
	 * Cron jobs
	 * Start lottery for users
	 * php /var/www/mil/console.php lottery
	 * @return array
	 */
	public function lottery()
	{
		//Check the date of lotteries
		$lottery_date = new \DateTime($this->model->get_settings("lottery_date"));
		$today = new \DateTime();

		//if lottery not today - do nothing
		if($lottery_date->format('Y-m-d') !== $today->format('Y-m-d')){
			return array(false, 'Lottery is not today');
		}

		$this->curr_date = $lottery_date->format('Y-m-d');

		$this->prev_date = $this->get_prev_lottery_date($lottery_date);

		//Update lotteries date
		//$this->set_new_lottery_date($lottery_date);

		$lottery_users = array();

		//Select the users.
		$lottery_total_count = $this->model->get_settings('lottery_total_count');

		//VIP-users
		$lottery_vips_count = $this->model->get_settings('lottery_vips_count');

		$lottery_vips_users = $this->get_lottery_users($lottery_vips_count, true);

		//Usual users
		$lottery_vips_count = count($lottery_vips_users);

		$lottery_usual_count = (int)$lottery_total_count - (int)$lottery_vips_count;

		$lottery_usual_users = $this->get_lottery_users($lottery_usual_count, false);

		//Get final user list
		$lottery_users = array_merge($lottery_users, $lottery_vips_users, $lottery_usual_users);

		//Write result into db
		$lottery_id = $this->model->add_new_lottery($this->curr_date);

		$this->model->set_lottery_winnes($lottery_id, $lottery_users);

		//Set curator
		$this->set_curator($lottery_id);

		//Send emails for winners
		foreach ($lottery_users as $lottery_user) {

			$user = $this->model->get_user_info($lottery_user);

			if(false !== $user){

				$message = \Swift_Message::newInstance()
					->setSubject('В отпуск миллионером! Вы выиграли!')
					->setFrom(array('info@bestlife-show.com'))
					->setTo(array($user['email']))
					->setBody($this->app['twig']->render('cron/winner.twig', $user));

				$this->app['swiftmailer.use_spool'] = false;

				$this->app['mailer']->send($message);
			}
		}

		return array(true, 'Done');
	}

	/**
	 * @param $users_count
	 * @param bool $vip
	 * @return array
	 */
	private function get_lottery_users($users_count, $vip = false)
	{
		$result = array();

		if($vip){
			$result = $this->model->lottery_vips($users_count);
		} else {
			$result = $this->model->lottery($users_count, $this->prev_date, $this->curr_date);
		}

		return $result;
	}

	private function set_curator($lottery_id)
	{
		//get first free curator
		$curator = $this->model->get_free_curator();

		if(false !== $curator){

			//write to db
			$this->model->set_lottery_curator($lottery_id, $curator['curator_id']);

			//Send emails for curator
			$message = \Swift_Message::newInstance()
				->setSubject('В отпуск миллионером! Вы выбраны куратором группы')
				->setFrom(array('info@bestlife-show.com'))
				->setTo(array($curator['email']))
				->setBody($this->app['twig']->render('cron/curator.twig', $curator));

			$this->app['swiftmailer.use_spool'] = false;

			$this->app['mailer']->send($message);

		} else {
			//TODO - что делать если нет свободного куратора ?
		}
	}

	/**
	 * @param \DateTime $current_date
	 * @return string
	 */
	private function get_prev_lottery_date(\DateTime $current_date)
	{
		$check_date = clone $current_date;

		$prev_date = $this->check_prev_date($check_date, false);

		if(false === $prev_date){

			//need check prev month
			$prev_date = $this->check_prev_date($check_date, true);
		}

		return $prev_date;
	}

	/**
	 * @param \DateTime $check_date
	 * @param bool $prev_month
	 * @return bool|string
	 */
	private function check_prev_date(\DateTime $check_date, $prev_month = false)
	{
		$result = false;

		$check_date_values = array(
			'month' => $check_date->format('m'),
			'year' => $check_date->format('Y'),
		);

		$lottery_dates = $this->model->get_settings("lottery_dates");

		$prev_date = new \DateTime($check_date_values['year'] . '-' . $check_date_values['month'] . '-' . reset($lottery_dates));

		if($prev_month) {
			$lottery_dates = array_reverse($lottery_dates);
		}

		foreach ($lottery_dates as $lottery_date) {

			$test_date = new \DateTime($check_date_values['year'] . '-' . $check_date_values['month'] . '-' . $lottery_date);

			if($prev_month) {

				$test_date->modify('- 1 month');

				if($check_date == $prev_date && $check_date > $test_date){

					$result = $test_date->format('Y-m-d');

					break;
				}
			} else {

				if($check_date > $prev_date && $check_date == $test_date){

					$result = $prev_date->format('Y-m-d');

					break;
				}
			}

			$prev_date = $test_date;
		}

		return $result;

	}

	/**
	 * @param \DateTime $current_date
	 */
	private function set_new_lottery_date(\DateTime $current_date)
	{
		$check_date = clone $current_date;

		$new_date = $this->check_new_date($check_date, false);

		if(false === $new_date){

			//need check next month
			$new_date = $this->check_new_date($check_date, true);
		}

		$this->model->set_settings("lottery_date", $new_date);
	}

	/**
	 * @param \DateTime $check_date
	 * @param bool $next_month
	 * @return bool|string
	 */
	private function check_new_date(\DateTime $check_date, $next_month = false)
	{
		$result = false;

		$check_date_values = array(
			'month' => $check_date->format('m'),
			'year' => $check_date->format('Y'),
		);

		$lottery_dates = $this->model->get_settings("lottery_dates");

		foreach ($lottery_dates as $lottery_date) {

			$test_date = new \DateTime($check_date_values['year'] . '-' . $check_date_values['month'] . '-' . $lottery_date);

			if($next_month){
				$test_date->modify('+ 1 month');
			}

			if($check_date < $test_date){

				$result = $test_date->format('Y-m-d');

				break;
			}
		}

		return $result;
	}

	/**
	 * Cron jobs
	 * Check users expiration data
	 * php /var/www/mil/console.php warning
	 * Preventing users from having to renew
	 * issued after X days ( Х * 24 * 60 * 60 = Х * 86400 seconds) from the date of registration
	 * @return array
	 */
	public function warningUsers()
	{
		//Calculate the time warning
		$warning_days = $this->model->get_settings("warning_days");
		
		$remove_days = $this->model->get_settings("remove_days");

		//Select the users who will be sent a warning
		$user_list = $this->model->get_warning_users((int)$warning_days * 86400);

		$users_ids_array = array();

		//We send letters
		foreach ($user_list as $user) {

			$user['warning_days'] = $warning_days;
			$user['remove_days'] = $remove_days;

			$message = \Swift_Message::newInstance()
				->setSubject('В отпуск миллионером! Обратите внимание')
				->setFrom(array('info@bestlife-show.com'))
				->setTo(array($user['email']))
				->setBody($this->app['twig']->render('cron/warning.twig', $user));

			$this->app['swiftmailer.use_spool'] = false;

			$this->app['mailer']->send($message);

			$users_ids_array[] = $user['id'];
		}

		//Change the field  `users`.`warning`
		if(0 !== count($users_ids_array)) {
			$this->model->set_warning_label($users_ids_array);
		}

		return array(true, 'Done');
	}

	/**
	 * Cron jobs
	 * Remove users with expired data
	 * php /var/www/mil/console.php remove
	 * Deleting users with expired subscription period.
	 * more than X days ( Х * 24 * 60 * 60 = Х * 86400 seconds) from the date of registration
	 * @return array
	 */
	public function removeUsers()
	{
		//We calculate the time of removal
		$remove_days = $this->model->get_settings("remove_days");

		//Select users to be removed
		$user_list = $this->model->get_remove_users((int)$remove_days * 86400);

		$users_ids_array = array();
		$users_photo_array = array();

		//Send letters
		foreach ($user_list as $user) {

			$user['remove_days'] = $remove_days;

			$message = \Swift_Message::newInstance()
				->setSubject('В отпуск миллионером! Благодарим за участие')
				->setFrom(array('info@bestlife-show.com'))
				->setTo(array($user['email']))
				->setBody($this->app['twig']->render('cron/remove.twig', $user));

			$this->app['swiftmailer.use_spool'] = false;

			$this->app['mailer']->send($message);

			$users_ids_array[] = $user['id'];

			if(!empty($user['photo'])){
				$users_photo_array[] = $user['photo'];
			}

		}

		//Delete unwanted photos
		if(0 !== count($users_photo_array)){
			$this->model->remove_user_photo($users_photo_array);
		}

		//Remove user accounts
		if(0 !== count($users_ids_array)){
			$this->model->remove_user_account($users_ids_array);
		}

		return array(true, 'Done');
	}

}
