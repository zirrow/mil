<?php
namespace controller;

use Silex\Application;
use Silex\Route;
use Symfony\Component\HttpFoundation\Request;

class pay {

	public function payStatus(Request $request, Application $app){

		$id = $request->get("ik_inv_id");
		$stat = $request->get("ik_inv_st");
		$no = $request->get("ik_pm_no");

		if(isset($id) && !empty($id)){
			$twig['id'] = $id;
		}

		if(isset($stat) && !empty($stat)){
			$twig['status'] = $stat;
		}

		if(isset($no) && !empty($no)){
			$twig['no'] = $no;
		}

		return $app['twig']->render('pay.twig',$twig);

	}

	public function payAdd(Request $request, Application $app){


			$program = new \model\pay;
			$result = $program->GetPayId($app);

			$id_co = $request->get("ik_co_id");

			if ($result == $id_co) {

				$no = $request->get("ik_pm_no");

				$val = explode('_', $no);

				$arr = array('user_id' => $val[0], 'ticket_id' => $val[1], 'date' => date("Y-m-d"));

				$program->addPay($arr, $app);

				return $app['twig']->render('index.twig');

			}
	}
}