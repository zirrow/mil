<?php
namespace controller;

use Silex\Application;
use Silex\Route;
use Symfony\Component\HttpFoundation\Request;

class index {

	public function home(Application $app){

		$user = $app['session']->get('user');
		$pass = $app['session']->get('pass');

		$twig['timer'] = '1';

		if(isset($user) && !empty($user) && isset($pass) && !empty($pass)){
			$twig['user'] = $app['session']->get('user');
			$twig['pass'] = $app['session']->get('pass');
		}

		return $app['twig']->render('index.twig',$twig);

	}

}