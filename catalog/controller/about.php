<?php
namespace controller;

use Silex\Application;
use Silex\Route;
use Symfony\Component\HttpFoundation\Request;

class about {

	public function index(Request $request, Application $app){


		return $app['twig']->render('about.twig');

	}

}