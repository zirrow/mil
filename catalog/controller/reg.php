<?php
namespace controller;

use Silex\Application;
use Silex\Route;
use Symfony\Component\HttpFoundation\Request;



class reg {

	public function index(Request $request, Application $app){

		if ($request->get("ref")){
			$app['session']->set('ref',$request->get("ref"));
		}

		return $app['twig']->render('reg.twig');

	}

	public function UserAdd(Request $request, Application $app){

		$lastName       = $request->get("lastName");
		$firstName      = $request->get("firstName");
		$dateBirthYear  = $request->get("dateBirth-year");
		$dateBirthMons  = $request->get("dateBirth-mons");
		$dateBirthDay   = $request->get("dateBirth-day");
		$inputEmail     = $request->get("inputEmail");
		$phoneNumber    = $request->get("phoneNumber");
		$agree          = $request->get("agree");
		$ref            = $app['session']->get('ref');

		if(isset($ref) && !empty($ref)){
			$addUser['ref'] = $ref;
			$twig['ref'] = $ref;
		} else {
			$addUser['ref'] = '';
		}

		if(isset($lastName) && !empty($lastName)){
			$lastName = htmlspecialchars($lastName);
			$addUser['lastName'] = $lastName;
			$twig['lastName'] = $lastName;
		} else {
			$twig['error']['lastName'] = 'error';
		}

		if(isset($firstName) && !empty($firstName)){
			$firstName = htmlspecialchars($firstName);
			$addUser['firstName'] = $firstName;
			$twig['firstName'] = $firstName;
		} else {
			$twig['error']['firstName'] = 'error';
		}

		if(isset($dateBirthYear) && !empty($dateBirthYear) && isset($dateBirthMons) && !empty($dateBirthMons)&& $dateBirthMons <= 12 && isset($dateBirthDay) && !empty($dateBirthDay) && $dateBirthDay <= 31){
			$dateBirth = htmlspecialchars($dateBirthYear).'-'.htmlspecialchars($dateBirthMons).'-'.htmlspecialchars($dateBirthDay);
			$addUser['dateBirth'] = $dateBirth;
			$twig['dateBirth'] = $dateBirth;
		} else {
			$twig['error']['dateBirth'] = 'error';
		}

		if(isset($inputEmail) && !empty($inputEmail)){
			$inputEmail = htmlspecialchars($inputEmail);
			$addUser['email'] = $inputEmail;
			$twig['email'] = $inputEmail;
		} else {
			$twig['error']['email'] = 'error';
		}

		if(isset($phoneNumber) && !empty($phoneNumber)){
			$phoneNumber = htmlspecialchars($phoneNumber);
			$addUser['phone'] = $phoneNumber;
			$twig['phone'] = $phoneNumber;
		} else {
			$twig['error']['phone'] = 'error';
		}

		if(!isset($agree)){
			$twig['error']['agree'] = 'error';
		}

		if (!isset($twig['error'])){

			$chars="qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP";
			$max=10;
			$size=StrLen($chars)-1;
			$password=null;
			while($max--){
				$password.=$chars[rand(0,$size)];
			}

			$twig['password']= $addUser['password'] = $password;

			$program = new \model\reg;
			$twig['userId'] = $program->newUser($addUser,$app);
			
			$app['session']->set('user',$twig['userId']);
			$app['session']->set('pass',$twig['password']);

			$message = \Swift_Message::newInstance()
				->setSubject('В отпуск миллионером! Спасибо за регистрацию')
				->setFrom(array('info@bestlife-show.com'))
				->setTo(array($inputEmail))
				->setBody($app['twig']->render('mail.twig',$twig));

			$app['mailer']->send($message);

			return $app->redirect($app['url_generator']->generate('suc'));

		}

		return $app['twig']->render('reg.twig',$twig);

	}

	public function Suc(Application $app){

		$twig['user'] = $twig['userId'] = $app['session']->get('user');
		$twig['pass'] = $twig['password'] = $app['session']->get('pass');

		return $app['twig']->render('suc.twig',$twig);

	}

	public function FindUser(Request $request, Application $app){

		$id        = $request->get("login");
		$pass      = $request->get("pass");

		if(isset($id) && !empty($id)){
			$id = htmlspecialchars($id);
		}

		if(isset($pass) && !empty($pass)){
			$pass = htmlspecialchars($pass);
		}

		if(isset($id)){

			if (!isset($pass) || empty($pass)) {
				$pass = '';
			}

			$program = new \model\reg;
			$result = $program->FindUserById($id,$pass,$app);

			if($result){

				if(empty($result['photo'])){
					$result['photo'] = SITE_NAME.'image/no_image.png';
				}

				if (!isset($pass) || empty($pass)) {
					$ix ='';
					for ($x=0; $x<strlen($result['email'])-4; $x++){
						$ix .= '*';
					}

					$result['email'] = substr_replace($result['email'],$ix,2, strlen($result['email'])-4);
				}else{
					$result['pass_isset'] = '1';
					$app['session']->set('pass',$pass);
					$twig['pass'] = $pass;
				}

				$twig['success'] = $result;
				$app['session']->set('user',$id);
				$twig['user'] = $id;
				$twig['paymetods'] = $this->GetPayMetods($id);
				$twig['cost'] = $this->GetCost($app);
				$twig['pay_id'] = $id.'_'.date_timestamp_get(date_create());

			}else{
				$twig['error'] = "log_pass";
			}

		} else {
			$twig['error'] = "isset";
		}

		return $app['twig']->render('edit.twig',$twig);

	}

	public function GetPayMetods($id){

		$result = file_get_contents('http://vkarpati.in.ua/api/gpm.html');

		$payMetodsArr = json_decode($result, true);

		return $payMetodsArr;
	}

	public function GetCost(Application $app){

		$program = new \model\reg;
		$price = $program->GetPice($app);

		$result = $price[0];
		return $result;
	}

	public function editUser(Request $request, Application $app){

		$id['id'] = $app['session']->get('user');

		$fatherName     = $request->get("fatherName");
		$pasport        = $request->get("pasport");
		$phoneNumber    = $request->get("phoneNumber");
		$country        = $request->get("country");
		$stars          = $request->get("stars");
		$assist         = $request->get("assist");
		$eat            = $request->get("eat");
		$diabet         = $request->get("diabet");
		$photo          = $request->get("photo");

		$addUser = array(
			'pasport'       => $pasport,
			'assist'        => $assist,
			'diabet'        => $diabet,
		);

		if(isset($fatherName) && !empty($fatherName)){
			$fatherName = htmlspecialchars(trim($fatherName));
			$addUser['fatherName'] = $fatherName;
		}

		if(isset($phoneNumber) && !empty($phoneNumber)){
			$phoneNumber = htmlspecialchars(trim($phoneNumber));
			$addUser['phone'] = $phoneNumber;
		}

		if(isset($country) && !empty($country)){
			$country = htmlspecialchars(trim($country));
			$addUser['country'] = $country;
		}

		if(isset($stars) && !empty($stars)){
			$stars = htmlspecialchars(trim($stars));
			$addUser['stars'] = $stars;
		}

		if(isset($eat) && !empty($eat)){
			$eat = htmlspecialchars(trim($eat));
			$addUser['eat'] = $eat;
		}

		if(isset($photo) && !empty($photo)){
			$addUser['photo'] = $photo;
		}

		$program = new \model\reg;
		$result = $program->editUserById($addUser,$id,$app);

		return $app->json($result, 200);

	}

	public function fotoUpload(){

		require_once __DIR__.'/../../vendor/fileupload/UploadHandler.php';

		$config = array(
			'upload_dir' => __DIR__.'/../../web/image/photos/',
			'upload_url' => '/image/photos/'
		);

		$upload_handler = new \UploadHandler($config);

		return false;

	}

}