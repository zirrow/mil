#!/usr/bin/env php
<?php
/**
 * Console tasks for cron
 */

require_once __DIR__ . '/config.php';

require_once __DIR__ . '/vendor/autoload.php';

//Initiate Silex
$app = new Silex\Application();

//Register a template provider
$app->register(new Silex\Provider\TwigServiceProvider(), array(
	'twig.path' => __DIR__ . '/catalog/view',
));

//Register a database provider
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
	'db.options' => array (
		'driver'    => 'pdo_mysql',
		'host'      => DB_HOST,
		'dbname'    => DB_NAME,
		'user'      => DB_USER,
		'password'  => DB_PASS,
		'charset'   => 'utf8mb4',
	),
));

//Register a sending email provider
$app->register(new Silex\Provider\SwiftmailerServiceProvider());

$app['swiftmailer.options'] = array(
	'host' => MAIL_HOST,
	'port' => MAIL_PORT,
	'username' => MAIL_USER,
	'password' => MAIL_PASS,
	'encryption' => null,
	'auth_mode' => null
);


//Register logging service
$app->register(new Silex\Provider\MonologServiceProvider(), array(
	'monolog.logfile' => DIR_ROOT.'/console.log',
));

//Include the namespaces of the components we plan to use
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

//Instantiate our Console application
$console = new Application('Console_tasks', '0.1');

//Register a command to run from the command line
//Our command will be started with "php /var/www/mil/console.php lottery"
$console->register( 'lottery' )
	->setDescription('Start lottery for users')
	->setHelp('Usage: <info>php /var/www/mil/console.php lottery</info>')
	->setCode(
		function(InputInterface $input, OutputInterface $output) use ($app)
		{
			//Task here
			$task = new controller\cron($app);

			$result = $task->lottery();

			$app['monolog']->info(sprintf("Task lottery complete. Status code: %s. Message: %s ", $result[0], $result[1]));

		}
	);

//Register a command to run from the command line
//Our command will be started with "php /var/www/mil/console.php warning"
$console->register( 'warning' )
	->setDescription('Check users expiration data')
	->setHelp('Usage: <info>php /var/www/mil/console.php warning</info>')
	->setCode(
		function(InputInterface $input, OutputInterface $output) use ($app)
		{
			//Task here
			$task = new controller\cron($app);

			$result = $task->warningUsers();

			$app['monolog']->info(sprintf("Task warning complete. Status code: %s. Message: %s ", $result[0], $result[1]));

		}
	);

//Register a command to run from the command line
//Our command will be started with "php /var/www/mil/console.php remove"
$console->register( 'remove' )
	->setDescription('Remove users with expired data')
	->setHelp('Usage: <info>php /var/www/mil/console.php remove</info>')
	->setCode(
		function(InputInterface $input, OutputInterface $output) use ($app)
		{
			//Task here
			$task = new controller\cron($app);

			$result = $task->removeUsers();

			$app['monolog']->info(sprintf("Task remove complete. Status code: %s. Message: %s ", $result[0], $result[1]));

		}
	);

$console->run();
